import React from 'react';

// Create a Context Object
// A Context Object as the name suggest is a data type of an object that can be used to store information that can be shared to other components within the app.
// Context object is a different approach in passing information betweeb components and allow easier access by avoiding props-drilling.
const UserContext = React.createContext();


// The "Provider" compnent allows other components to consume/ use the context object and supply the necessary information needed to the context object
export const UserProvider = UserContext.Provider;

export default UserContext;
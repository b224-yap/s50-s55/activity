import { useState, useEffect } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function CourseCard ({courseProp}) {

	// Checks to see if the data was successfully passed.
	// console.log(props)
	// Every component receives information in the form of objects
	// console.log(typeof props)
	// Using the dot notation, access the property to retrieve the value of data.value
	// console.log(props.courseProp.name)
	// Checks if we can retrieve data from courseProp
	console.log(courseProp)


	/*
		Use the state hook for this component to be able to store its state.
		States are used to keep track of the information re;ated to individual components

		Syntax:
			const [getter, setter] = useState(initialGetterValue)
	*/

	const [count, setCount] = useState(0);
	// console.log(useState(0));
	const [seats, setSeats] = useState(30)

	// function enroll () {
	// 	if(count < 30 ){
	// 		setCount(count + 1)
	// 		console.log('Enrollee:' + count)
	// 		setCount1(count1 - 1)
	// 		console.log('seats: ' + count1)
	// 	} else {
	// 		alert("no more seats Available");
	// 	}	
	// }

	function enroll() {
			if(count < 30) {
				setCount(count + 1)
				// console.log('Enrollee: ' + count)
				setSeats(seats - 1)
				// console.log('Seats: ' + seats)
			} //else {
			// 	alert("No more seats available.")
			// }
		}

		useEffect(()=>{
			if(seats === 0){
				alert("No more seats availabe.")
			}
		}, [seats])

const { name, description, price, _id } = courseProp;

	return (

		<Row className="mt-3 mb-3">
			<Col xs={12} md={6}>
				<Card className="coursCard p-3">
				      <Card.Body>
				        <Card.Title>{name}</Card.Title>
				        <h6>Description</h6>
				        <p>{description}</p>
				        <h6>Price:</h6>
				        <h6>PhP {price}</h6>
				        
				        <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
				      </Card.Body>
				    </Card>
			</Col>
		</Row>

	)
}